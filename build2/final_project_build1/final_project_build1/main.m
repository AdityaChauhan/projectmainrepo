//
//  main.m
//  final_project_build1
//
//  Created by Aditya on 4/16/15.
//  Copyright (c) 2015 Adie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
