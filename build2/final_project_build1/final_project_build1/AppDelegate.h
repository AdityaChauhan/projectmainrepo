//
//  AppDelegate.h
//  final_project_build1
//
//  Created by Aditya on 4/16/15.
//  Copyright (c) 2015 Adie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

