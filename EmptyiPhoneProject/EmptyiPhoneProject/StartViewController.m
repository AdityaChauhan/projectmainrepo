//
//  StartViewController.m
//  EmptyiPhoneProject
//
//  Copyright (c) 2014 CS3200. All rights reserved.
//

#import "StartViewController.h"
#import "uploadViewController.h"

@interface StartViewController ()

@property (nonatomic, strong) UIImageView *capturedImage;

@end

@implementation StartViewController

-(instancetype)init
{
	self = [super init];
	if(self)
	{
		self.view.backgroundColor = [UIColor colorWithRed:0.29 green:0.91 blue:1 alpha:1];
        
        UIButton *uploadBtn = [[UIButton alloc] initWithFrame:CGRectMake(50, 80, 100, 40)];
        uploadBtn.backgroundColor = [UIColor colorWithRed:1 green:0.565 blue:0.29 alpha:1];
        [uploadBtn setTitle:@"Upload" forState:UIControlStateNormal];
        [uploadBtn addTarget:self action:@selector(displayUploadViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:uploadBtn];
        
        /*self.capturedImage = [[UIImageView alloc] initWithFrame:CGRectMake(50, 50, 200, 200)];
        self.capturedImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:self.capturedImage];
        
        UIButton *cameraBtn = [[UIButton alloc] initWithFrame:CGRectMake(50, 300, 100, 40)];
        cameraBtn.backgroundColor = [UIColor blueColor];
        [cameraBtn addTarget:self action:@selector(displayCamera) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:cameraBtn];*/
                              
		
		
	}
	return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

-(void)displayUploadViewController
{
    // make sure to include your new View Controller class!
    // create it and push it onto your navigation controller stack
    
    uploadViewController *uploadView = [[uploadViewController alloc] init];
    [self.navigationController pushViewController:uploadView animated:YES];
    
}

/*-(void)displayCamera
{
    // check to make sure the device has a camera
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        // create a UIImagePickerController and set it's source type to the camera
        UIImagePickerController *camera = [[UIImagePickerController alloc] init];
        camera.delegate = self;	// for the delegate you need: UIImagePickerControllerDelegate and UINavigationControllerDelegate
        camera.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self.navigationController presentViewController:camera animated:YES completion:nil];
    }
    else
    {
        // show an alert that they don't have a camera
        UIAlertView *alertNoCamera = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device has no camera" delegate:nil
                                                      cancelButtonTitle:@"okay" otherButtonTitles:nil];
        [alertNoCamera show];
    }
}

// this is a method on the protocol/delegate "UIImagePickerControllerDelegate"
// it will be called when the UIImagePickerController selected an image
// the image information is inside the info dictionary
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *capturedImage = [info objectForKey:UIImagePickerControllerOriginalImage];	// get the image they took/selected, use the key "UIImagePickerControllerOriginalImage"
    
    self.capturedImage.image = capturedImage;
    
    NSLog(@"picked an image");
}*/


@end
