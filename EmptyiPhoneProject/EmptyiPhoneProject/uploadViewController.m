//
//  uploadViewController.m
//  EmptyiPhoneProject
//
//  Created by Aditya on 4/16/15.
//  Copyright (c) 2015 CS3200. All rights reserved.
//

#import "uploadViewController.h"
#import "StartViewController.h"
@interface uploadViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIImageView *capturedImage;

@end

@implementation uploadViewController

-(instancetype) init
{
    self = [super init];
    if(self)
    {
        self.view.backgroundColor = [UIColor colorWithRed:0.6 green:0.98 blue:1 alpha:1];
        
        /*UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(100, 100, 100, 40)];
        [backButton setTitle:@"Back" forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.view addSubview:backButton];
        backButton.userInteractionEnabled = NO;
        
        // moving a frame
        CGRect currentFrame = backButton.frame;
        currentFrame.origin.x = 300;
        backButton.frame = currentFrame;*/
        
        self.capturedImage = [[UIImageView alloc] initWithFrame:CGRectMake(100, 350, 450, 300)];
        self.capturedImage.backgroundColor = [UIColor colorWithRed:1 green:0.565 blue:0.29 alpha:1];
        self.capturedImage.contentMode = UIViewContentModeScaleToFill;
        [self.view addSubview:self.capturedImage];
        
        UIButton *cameraBtn = [[UIButton alloc] initWithFrame:CGRectMake(100, 250, 150, 50)];
        [cameraBtn setTitle:@"Use Camera" forState:(UIControlStateNormal)];
        cameraBtn.backgroundColor = [UIColor colorWithRed:1 green:0.565 blue:0.29 alpha:1];
        [cameraBtn addTarget:self action:@selector(displayCamera) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:cameraBtn];
        
        UIButton *useGalleryBtn = [[UIButton alloc] initWithFrame:CGRectMake(400, 250, 150, 50)];
        [useGalleryBtn setTitle:@"Use Gallery" forState:UIControlStateNormal];
        useGalleryBtn.backgroundColor = [UIColor colorWithRed:1 green:0.565 blue:0.29 alpha:1];
        [useGalleryBtn addTarget:self action:@selector(selectPhoto:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:useGalleryBtn];
        
        UIButton *savePic = [[UIButton alloc] initWithFrame:CGRectMake(600, 250, 150, 50)];
        [savePic setTitle:@"Save" forState:UIControlStateNormal];
        savePic.backgroundColor = [UIColor colorWithRed:1 green:0.565 blue:0.29 alpha:1];
        [savePic addTarget:self action:@selector(SavePhotoOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:savePic];

        
    }
    return self;
}

//code to go to gallery and choose pics
- (IBAction)selectPhoto:(UIButton *)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)goBack
{
    // to pop this view off manually, call "popViewControllerAnimated:" on your naviation contorller
    [self.navigationController popViewControllerAnimated:YES];
}

// this is called right before the view will be displayed
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}
/*//delegate for image picker
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
    self.capturedImage.image = selectedImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}*/
//if somebody presses cancel button
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
//Code to save the picture in phone
- (void)SavePhotoOnClick:(id)sender{
    if ((self.capturedImage.image == NULL)) {
        UIAlertView *alertNoPic = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please choose a picture." delegate:nil cancelButtonTitle:@"Okay!" otherButtonTitles: nil];
        [alertNoPic show];
    }
    else{
        UIImageWriteToSavedPhotosAlbum(self.capturedImage.image, nil, nil, nil);
    }
}
-(void)displayCamera
{
    // check to make sure the device has a camera
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        // create a UIImagePickerController and set it's source type to the camera
        UIImagePickerController *camera = [[UIImagePickerController alloc] init];
        camera.delegate = self;	// for the delegate you need: UIImagePickerControllerDelegate and UINavigationControllerDelegate
        camera.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self.navigationController presentViewController:camera animated:YES completion:nil];
    }
    else
    {
        // show an alert that they don't have a camera
        UIAlertView *alertNoCamera = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device has no camera" delegate:nil
                                                      cancelButtonTitle:@"okay" otherButtonTitles:nil];
        [alertNoCamera show];
    }
}

// this is a method on the protocol/delegate "UIImagePickerControllerDelegate"
// it will be called when the UIImagePickerController selected an image
// the image information is inside the info dictionary
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
    self.capturedImage.image = selectedImage;
   
    UIImage *capturedImage = [info objectForKey:UIImagePickerControllerOriginalImage];	// get the image they took/selected, use the key "UIImagePickerControllerOriginalImage"
    
    self.capturedImage.image = capturedImage;
    
     [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"picked an image");
}

@end

