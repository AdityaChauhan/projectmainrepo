//
//  AppDelegate.h
//  EmptyiPhoneProject
//
//  Copyright (c) 2014 CS3200. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

